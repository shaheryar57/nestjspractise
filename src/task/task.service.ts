import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { filterTaskDTo } from './dto/filter-task-dto';
import { Task, TaskStatus } from './task.model';

@Injectable()
export class TaskService {
    private tasks : Task[] = [];

    getAllTasks() : Task[]
    {
        return this.tasks;
    }

    filterTasks (filterTaskDto : filterTaskDTo): Task[]
    {
           const {search, status} = filterTaskDto;

           let tasks = this.getAllTasks();

           if(status)
           {
            tasks = tasks.filter(task => task.status === status)
           }

           if(search)
           {
               tasks = tasks.filter (task =>
                task.title.includes(search) || 
                task.description.includes(search),
                )
           }
           return tasks
    }

    getSpecificTaskbyId(id : string) : Task
    {
        const task = this.tasks.find(task => task.id === id)
       if(task)
       {
          return task;
       }
       else
       {
           throw new NotFoundException(`task with ${id} does not exist!`);
       }

        
    }

    deleteSpecificTask (id: string) : Task[]
    {
        const tasksAfterDeletingOne = this.tasks.filter((task)=>{
            return task.id !== id;
        })
        this.tasks = tasksAfterDeletingOne;
       return tasksAfterDeletingOne;

    }

    updateSpecificTask (id: string, status:TaskStatus): Task
    {
        const findTaskToUpdate = this.getSpecificTaskbyId(id);
         findTaskToUpdate.status = status;
         return findTaskToUpdate
    }

    createNewTask (createTaskDto) : Task {
        const {title, description} = createTaskDto;
        const task : Task ={
            id: uuidv4(),
            title,
            description,
            status: TaskStatus.DONE
        }
         this.tasks.push(task);
         return task;
    }
}
