import { Body, Controller, Get, Param, Post,Delete, Patch, UsePipes, ValidationPipe } from '@nestjs/common';
import { createTaskDto } from './dto/create-task-dto';
import { filterTaskDTo } from './dto/filter-task-dto';
import { Task, TaskStatus } from './task.model';
import { TaskService } from './task.service';

@Controller('task')
export class TaskController {
    constructor(private taskService: TaskService){}

    @Get()

    getTasks(@Param() filterTaskDTo : filterTaskDTo) : Task[]{
        if(Object.keys(filterTaskDTo).length)
        {
            return this.taskService.filterTasks(filterTaskDTo)
        }
        else{
            return this.taskService.getAllTasks();
        }
       
    }

   @Get('/:id')
   getSpecificTaskbyId (@Param('id') id: string) : Task
   {
     return this.taskService.getSpecificTaskbyId(id)
   }

 
    @Post()
    @UsePipes(ValidationPipe) // it validation Pipe it will take the entire req body!
    createTask (@Body()  createTaskDto : createTaskDto) : Task {
        return this.taskService.createNewTask(createTaskDto);
    }

    @Delete('/:id')
    deleteSpecificTask (@Param('id') id : string)
    {
        return this.taskService.deleteSpecificTask(id)

    }

    @Patch('/:id/status')
    updateSpecificTask (@Param('id') id : string, @Body('status') status : TaskStatus) : Task
    {
     return this.taskService.updateSpecificTask(id, status)
    }

}
